# pico-w-breakout-garden

Inspired by Twitter post from [DanielDimmick](https://twitter.com/DanielDimmick/status/1544652315684225025).

<img src="https://pbs.twimg.com/media/FW-1GDjXoAEtxcb?format=jpg&name=large" width="50%"/>

And rehacked:

<img src="./img.jpg" width="50%"/>

## TODO

- [ ] publish sensor data to mqtt. currently mqtt is not part of `main.py`

## Prerequisites

- MQTT Server with user and password in your network
- Hardware:
  - 1x Raspberry Pi Pico W - Pico W
  - 1x Pico Breakout Garden Base
  - 1x 1.54" SPI Colour Square LCD (240x240) Breakout
  - 1x LED Dot Matrix Breakout - Red
  - 1x RV3028 Real-Time Clock (RTC) Breakout
  - 1x BME688 4-in-1 Air Quality Breakout (Gas, Temperature, Pressure, Humidity)
  - (optional) 1x MSA301 3DoF Motion Sensor Breakout
    - not part of the main programm (`main.py`), but output will be shown on console with `pico/examples/msa_demo.py`

## Setting up

Get firmware

- from <https://github.com/pimoroni/pimoroni-pico/releases>
  - download `pimoroni-picow-v1.19.9-micropython.uf2`

Install to Pico

- Hold BOOTSEL Button
- Connect USB Cable to PC
- copy `*.uf2` file to pico

Install Thonny to PC

- from <https://thonny.org/>
- open Thonny

Configure Thonny

- go to `Tools > Options > Interpreter`
- choose Interpreter `MicroPython (Raspberry Pi Pico)` and Port `USB Serical Device (COM3)`

Transfer Files to Pico using Thonny

- go to `View > Files`
- navigate with `Files > This computer` to pico-w-breakout-garden folder
- on computer: open `pico/` folder and upload all contents to pico which includes:
  - `example/`
  - `lib/`
  - `setup/`
  - `config-mqtt.txt`
  - `config-wlan.txt`
  - `main.py`
- on pico: edit files `config-wlan.txt`, `config-mqtt.txt`

Setup RTC (only needed once)

- on pico: open `setup/rtc_set_time.py` and run it
- Output:
  ```plain
  Getting time from Pico RTC/Thonny: (2022, 11, 5, 5, 10, 58, 20, 0)
  Setting the breakout RTC!
  New breakout time: 05/11/2022 10:58:20
  ```

Start `main.py`

- on pico: open `main.py` and run it

## Related articles

- [pimoroni-pico - example code and firmware](https://github.com/pimoroni/pimoroni-pico)
- [pimoroni - getting started with pico](https://learn.pimoroni.com/article/getting-started-with-pico)

## License

Copyright 2022 https://github.com/lixhunter.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
