import math
from lib.typecheck import typecheck
import lib.rtc as rtc
import lib.led as led
import lib.matrix_display as matrix_display
import lib.display as display
import lib.network as network
import lib.bme as bme


def init() -> None:
    rtc.init()
    led.init()
    matrix_display.init()
    display.init()
    display.display.set_font("bitmap8")


async def view(network_ok_event, sensor) -> None:
    init()
    display_margin = 10
    while True:
        display.reset()
        # class RTC: https://docs.micropython.org/en/latest/library/pyb.RTC.html
        display.display.set_pen(display.red_pen)
        wifitext: str = 'WiFi Disconnected'
        if network_ok_event.is_set():
            display.display.set_pen(display.green_pen)
            wifitext: str = 'WiFi Connected'
        text: str = f"{wifitext}"
        typecheck(text, str)
        display.display.text(text=text, x1=0+display_margin, y1=0+display_margin, scale=display.font_scale)
        display.display.set_pen(display.white_pen)
        rtc: tuple = machine.RTC().datetime(); typecheck(rtc, tuple)
        datetime: str = f'{rtc[0]}/{("%02d" % (rtc[1]))}/{("%02d" % (rtc[2]))} {("%02d" % (rtc[4]))}:{("%02d" % (rtc[5]))}:{("%02d" % (rtc[6]))}'
        typecheck(datetime, str)
        sen_temp = sensor.temperature if sensor.temperature else 0.0
        sen_press = sensor.pressure if sensor.pressure else 0.0
        sen_hum = sensor.humidity if sensor.humidity else 0.0
        text: str = \
f"""\
Time: {datetime}\n
\n
MQTT Last Published\n
06/07/2022 12:57:55\n
\n
Temp: {"%.2f" % sen_temp} °C\n
Pressure: {"%.2f" % (sen_press / 100)} hPa\n
Humidity: {"%.2f" % sen_hum} %\n
\n
Count: 35207"""
        typecheck(text, str)
        display.display.text(text=text, x1=0+display_margin, y1=math.floor(17*display.font_scale)+display_margin, scale=display.font_scale)
        display.display.update()
        led.led.toggle()
        await uasyncio.sleep_ms(1000)

async def main() -> None:
    import uasyncio
    from uasyncio import Event
    network_ok_event = Event()
    uasyncio.create_task(network.network(network_ok_event))
    sensor = bme.Sensor()
    uasyncio.create_task(bme.measure(sensor))
    uasyncio.create_task(view(network_ok_event, sensor))
    while True:
        await uasyncio.sleep_ms(500)


if __name__ == '__main__':
    import uasyncio
    uasyncio.run(main())
