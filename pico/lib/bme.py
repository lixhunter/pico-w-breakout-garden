def init():
    global bmp
    from breakout_bme68x import BreakoutBME68X, STATUS_HEATER_STABLE
    from pimoroni_i2c import PimoroniI2C

    PINS_BREAKOUT_GARDEN = {"sda": 4, "scl": 5}
    PINS_PICO_EXPLORER = {"sda": 20, "scl": 21}

    i2c = PimoroniI2C(**PINS_BREAKOUT_GARDEN)

    bmp = BreakoutBME68X(i2c)


async def update(sensor):
    import uasyncio
    from breakout_bme68x import STATUS_HEATER_STABLE
    while True:
        temperature, pressure, humidity, gas, status, _, _ = bmp.read()
        heater = "Stable" if status & STATUS_HEATER_STABLE else "Unstable"
        #print("{:0.2f}c, {:0.2f}Pa, {:0.2f}%, {:0.2f} Ohms, Heater: {}".format(
        #    temperature, pressure, humidity, gas, heater))
        sensor.temperature = temperature - 2.5
        sensor.pressure = pressure
        sensor.humidity = humidity
        sensor.gas = gas
        sensor.heater = heater
        await uasyncio.sleep_ms(1_000)


async def measure(sensor):
    import uasyncio
    init()
    uasyncio.create_task(update(sensor))
    while True:
        await uasyncio.sleep_ms(500)


class Sensor:
    temperature = None
    pressure = None
    humidity = None
    gas = None
    heater = None
    aux1 = None
    aux2 = None


if __name__ == '__main__':
    import uasyncio
    sensor = Sensor()
    uasyncio.run(measure(sensor))
