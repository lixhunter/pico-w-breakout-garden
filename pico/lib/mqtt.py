def sub_cb(topic, msg):
  print((topic, msg))
  if topic == b'notification' and msg == b'received':
    print('ESP received hello message')


def connect():
    import network
    import umqttsimple
    from ubinascii import hexlify
    global client
    print('Starting MQTT Client')
    client_id = hexlify(machine.unique_id())
    topic_sub = b'notification'
    last_message = 0
    message_interval = 5
    counter = 0
    with open('config-mqtt.txt') as f:
        mqtt_server, mqtt_port, mqtt_user, mqtt_password = f.read().splitlines()
    client = umqttsimple.MQTTClient(client_id=client_id, server=mqtt_server, port=int(mqtt_port), user=mqtt_user, password=mqtt_password)
    client.set_callback(sub_cb)
    client.connect()
    client.subscribe(topic_sub)
    print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
    return client


def restart_and_reconnect():
    import time
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()


async def main():
    # helpful - MicroPython – Getting Started with MQTT on ESP32/ESP8266:
    # https://randomnerdtutorials.com/micropython-mqtt-esp32-esp8266/
    import lib.network
    import uasyncio
    import time
    from uasyncio import Event
    network_ok_event = Event()
    uasyncio.create_task(lib.network.network(network_ok_event))
    await network_ok_event.wait()
    try:
        client = connect()
    except OSError as e:
        restart_and_reconnect()
    topic_pub = b'notification'
    while True:
        try:
            await network_ok_event.wait()
            new_message = client.check_msg()
            if new_message != 'None':
                await network_ok_event.wait()
                client.publish(topic_pub, b'received')
            await uasyncio.sleep_ms(1_000)
        except OSError as e:
            restart_and_reconnect()


if __name__ == '__main__':
    import uasyncio
    uasyncio.run(main())
