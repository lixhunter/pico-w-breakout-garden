def typecheck(instance, classes: tuple) -> None:
    assert isinstance(instance, classes), f'typecheck failed. type was "{instance.__class__.__name__}"'


if __name__ == '__main__':
    # this should be successful:
    text: str = 'hello world'
    typecheck(text, str)

    # this should fail:
    # micropython does not typecheck, so you can create a
    # string and try to statically type it as int.
    text: int = 'hello world'
    # but if you check the type using isinstance, then it fails:
    typecheck(text, int)
