def connect():
    import network
    global wlan
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    # works when __main__
    with open('config-wlan.txt') as f:
        ssid, password = f.read().splitlines()
    wlan.connect(ssid, password)

async def connectionMonitor(network_ok_event):
    import uasyncio
    while True:
        if wlan.isconnected():
            if not network_ok_event.is_set():
                print(wlan.ifconfig())
                print('Setting connection state to True')
                network_ok_event.set()
        else:
           if network_ok_event.is_set():
                print('Setting connection state to False')
                network_ok_event.clear()
        await uasyncio.sleep_ms(500)


async def network(network_ok_event):
    import uasyncio
    connect()
    uasyncio.create_task(connectionMonitor(network_ok_event))
    while True:
        await uasyncio.sleep_ms(500)

if __name__ == '__main__':
    import uasyncio
    from uasyncio import Event
    network_ok_event = Event()
    uasyncio.run(network(network_ok_event))
