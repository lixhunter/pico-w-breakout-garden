def init() -> None:
    import machine
    from lib.typecheck import typecheck
    global led
    led: machine.Pin = machine.Pin('LED', machine.Pin.OUT); typecheck(led, machine.Pin)


if __name__ == '__main__':
    import time
    init()
    while True:
        led.toggle()
        time.sleep(2)
