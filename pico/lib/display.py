def init() -> None:
    from picographics import PicoGraphics, DISPLAY_LCD_240X240, PEN_P8
    from lib.typecheck import typecheck
    global display, font_scale, background_pen, white_pen, green_pen, red_pen
    # PicoGraphics: https://github.com/pimoroni/pimoroni-pico/blob/main/micropython/modules/picographics/README.md
    display: PicoGraphics = PicoGraphics(display=DISPLAY_LCD_240X240, pen_type=PEN_P8); typecheck(display, PicoGraphics)
    display.set_backlight(1.0)
    red: int = 0; typecheck(red, int)
    green: int = 0; typecheck(green, int)
    blue: int = 0; typecheck(blue, int)
    background_pen: int = display.create_pen(red, green, blue); typecheck(background_pen, int)
    font_scale: float = 2.0; typecheck(font_scale, float)
    white_pen = display.create_pen(255, 255, 255); typecheck(white_pen, int)
    green_pen = display.create_pen(0, 255, 0); typecheck(green_pen, int)
    red_pen = display.create_pen(255, 0, 0); typecheck(green_pen, int)
    display.set_font("bitmap8")


def reset() -> None:
    display.set_pen(background_pen)
    display.clear()


if __name__ == '__main__':
    import time
    init()
    while True:
        display.set_pen(green_pen)
        display.text(text="hello world", x1=50, y1=50, scale=font_scale)
        display.update()
        time.sleep(0.5)
        reset()
        display.set_pen(white_pen)
        display.text(text="hello world", x1=50, y1=100, scale=font_scale)
        display.update()
        time.sleep(0.5)
        reset()
