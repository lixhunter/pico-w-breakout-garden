def init() -> None:
    from pimoroni_i2c import PimoroniI2C
    from breakout_dotmatrix import BreakoutDotMatrix
    from lib.typecheck import typecheck
    global matrix_display
    PINS_BREAKOUT_GARDEN: dict = {"sda": 4, "scl": 5}; typecheck(PINS_BREAKOUT_GARDEN, dict)
    PINS_PICO_EXPLORER: dict = {"sda": 20, "scl": 21}; typecheck(PINS_PICO_EXPLORER, dict)
    address: int = 0x61; typecheck(address, int)
    i2c: PimoroniI2C = PimoroniI2C(**PINS_BREAKOUT_GARDEN); typecheck(i2c, PimoroniI2C)
    # BreakoutDotMatrix: https://github.com/pimoroni/pimoroni-pico/tree/main/micropython/examples/breakout_dotmatrix
    matrix_display: BreakoutDotMatrix = BreakoutDotMatrix(i2c, address=address); typecheck(matrix_display, BreakoutDotMatrix)
    matrix_display.clear()
    left :str = 'o'; typecheck(left, str)
    right :str = 'k'; typecheck(right, str)
    matrix_display.set_character(0, ord(left))
    matrix_display.set_character(5, ord(right))
    matrix_display.show()


if __name__ == '__main__':
    import time
    init()
    while True:
        matrix_display.clear()
        matrix_display.set_character(5, ord('m'))
        matrix_display.show()
        time.sleep(0.3)
        matrix_display.set_character(0, ord('m'))
        matrix_display.set_character(5, ord('a'))
        matrix_display.show()
        time.sleep(0.3)
        matrix_display.set_character(0, ord('a'))
        matrix_display.set_character(5, ord('i'))
        matrix_display.show()
        time.sleep(0.3)
        matrix_display.set_character(0, ord('i'))
        matrix_display.set_character(5, ord('n'))
        matrix_display.show()
        time.sleep(0.3)
        matrix_display.clear()
        matrix_display.set_character(0, ord('n'))
        matrix_display.show()
        time.sleep(0.3)
