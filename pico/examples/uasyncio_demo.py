import uasyncio
import lib.matrix_display as matrix_display
import lib.display as display

async def async_blink(led, period_ms):
    while True:
        led.on()
        await uasyncio.sleep_ms(5)
        led.off()
        await uasyncio.sleep_ms(period_ms)


async def async_matrix_display():
    matrix_display.init()
    while True:
        matrix_display.matrix_display.clear()
        matrix_display.matrix_display.set_character(5, ord('m'))
        matrix_display.matrix_display.show()
        await uasyncio.sleep_ms(300)
        matrix_display.matrix_display.set_character(0, ord('m'))
        matrix_display.matrix_display.set_character(5, ord('a'))
        matrix_display.matrix_display.show()
        await uasyncio.sleep_ms(300)
        matrix_display.matrix_display.set_character(0, ord('a'))
        matrix_display.matrix_display.set_character(5, ord('i'))
        matrix_display.matrix_display.show()
        await uasyncio.sleep_ms(300)
        matrix_display.matrix_display.set_character(0, ord('i'))
        matrix_display.matrix_display.set_character(5, ord('n'))
        matrix_display.matrix_display.show()
        await uasyncio.sleep_ms(300)
        matrix_display.matrix_display.clear()
        matrix_display.matrix_display.set_character(0, ord('n'))
        matrix_display.matrix_display.show()
        await uasyncio.sleep_ms(300)

async def async_display():
    display.init()
    while True:
        display.display.set_pen(display.green_pen)
        display.display.text(text="hello world", x1=50, y1=50, scale=display.font_scale)
        display.display.update()
        await uasyncio.sleep_ms(500)
        display.reset()
        display.display.set_pen(display.white_pen)
        display.display.text(text="hello world", x1=50, y1=100, scale=display.font_scale)
        display.display.update()
        await uasyncio.sleep_ms(500)
        display.reset()
        

async def main(led1):
    print('start task 1.')
    uasyncio.create_task(async_blink(led1, 700))
    await uasyncio.sleep_ms(3_000)
    print('start task 2.')
    uasyncio.create_task(async_matrix_display())
    await uasyncio.sleep_ms(3_000)
    print('start task 3.')
    uasyncio.create_task(async_display())
    print('waiting...')
    await uasyncio.sleep_ms(10_000)
    # cleanup
    print('cleanup.')
    led1.off()
    matrix_display.matrix_display.clear()
    matrix_display.matrix_display.show()
    display.reset()
    display.display.update()
    display.display.set_backlight(0.0)


import machine
led = machine.Pin('LED', machine.Pin.OUT)
uasyncio.run(main(led))
